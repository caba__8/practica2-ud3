package com.alecaba.surf.gui;

import com.alecaba.surf.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/***
 * Clase Modelo
 */
public class Modelo {

    SessionFactory sessionFactory;

    /***
     * Método desconectar
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /***
     * Método conectar
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Marca.class);
        configuracion.addAnnotatedClass(ClienteProducto.class);
        configuracion.addAnnotatedClass(ClienteTienda.class);
        configuracion.addAnnotatedClass(VentaProducto.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /***
     * Devuelve los productos que tiene una marca
     * @param marca marca
     * @return lista de productos
     */
    ArrayList<Producto> getMarcaProductos(Marca marca) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE marca =: edit");
        query.setParameter("edit", marca);
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }

    /***
     * Devuelve los productos disponibles
     * @return lista de productos
     */
    ArrayList<Producto> getProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto");
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }


    /***
     * Devuelve los clientes disponibles
     * @return lista de clientes
     */
    ArrayList<Cliente> getClientes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente");
        ArrayList<Cliente> listaClientes = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaClientes;
    }

    /***
     * Devuelve las tiendas disponibles
     * @return lista de tiendas
     */
    ArrayList<Tienda> getTiendas(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Tienda");
        ArrayList<Tienda> listaTiendas = (ArrayList<Tienda>)query.getResultList();
        sesion.close();
        return listaTiendas;
    }

    /***
     * Devuelve las marcas disponibles
     * @return lista de marcas
     */
    ArrayList<Marca> getMarcas(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Marca");
        ArrayList<Marca> listaMarcas = (ArrayList<Marca>)query.getResultList();
        sesion.close();
        return listaMarcas;
    }

    /***
     * Devuelve las ventas disponibles
     * @return lista de ventas
     */
    ArrayList<VentaProducto> getVentas(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM VentaProducto");
        ArrayList<VentaProducto> ventas = (ArrayList<VentaProducto>)query.getResultList();
        sesion.close();
        return ventas;
    }

    /***
     * Insertar un objeto en la BBDD
     * @param o objeto a insertar en la BBDD
     */
    void insertar(Object o) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(o);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /***
     * Modificar un objeto de la BBDD
     * @param o objeto a modificar en la BBDD
     */
    void modificar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(o);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /***
     * Eliminar un objeto de la BBDD
     * @param o objeto a eliminar en la BBDD
     */
    void eliminar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(o);
        sesion.getTransaction().commit();
        sesion.close();
    }
}
