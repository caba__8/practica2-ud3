package com.alecaba.surf.gui;

import com.alecaba.surf.base.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/***
 * Clase controlador
 */
public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;
    private boolean conectado;

    /***
     * Constructor de la clase
     * @param modelo modelo
     * @param vista vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;
        this.conectado = false;

        addActionListeners(this);
        addListSelectionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        if (!conectado && !comando.equalsIgnoreCase("Conectar")) {
            JOptionPane.showMessageDialog(null, "No has conectado con la BBDD",
                    "Error de conexión", JOptionPane.ERROR_MESSAGE);
            return;
        }
        switch (comando) {
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                conectado = true;
                break;

            case "altaProductoBtn":
                Producto nuevoProducto = new Producto();
                nuevoProducto.setNombre(vista.txtNombre.getText());
                nuevoProducto.setCodigo(vista.txtCodigo.getText());
                nuevoProducto.setMarca((Marca) vista.comboMarcas.getSelectedItem());
                modelo.insertar(nuevoProducto);
                break;

            case "listarProductos":
                listarProductos(modelo.getProductos());
                break;

            case "modificarProductoBtn": {
                Producto p = (Producto) vista.listProductos.getSelectedValue();
                p.setNombre(vista.txtNombre.getText());
                p.setCodigo(vista.txtCodigo.getText());
                p.setMarca((Marca) vista.comboMarcas.getSelectedItem());
                modelo.modificar(p);
                break;
            }

            case "eliminarProductoBtn": {
                Producto p = (Producto) vista.listProductos.getSelectedValue();
                if (!comprobarProductoVenta(p.getIdproducto())) {
                    JOptionPane.showMessageDialog(null, "Este producto está ligado a una venta, elimina primero la venta.",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                modelo.eliminar(p);
                break;
            }
            case "altaClienteBtn": {
                Cliente c = new Cliente();
                c.setNombre(vista.txtNombreCliente.getText());
                c.setApellidos(vista.txtApellidos.getText());
                c.setDni(vista.txtDNI.getText());
                modelo.insertar(c);
                break;
            }
            case "modificarClienteBtn": {
                Cliente c = (Cliente) vista.listClientes.getSelectedValue();
                c.setNombre(vista.txtNombreCliente.getText());
                c.setApellidos(vista.txtApellidos.getText());
                c.setDni(vista.txtDNI.getText());
                modelo.modificar(c);
            }
            break;
            case "eliminarClienteBtn": {
                Cliente c = (Cliente) vista.listClientes.getSelectedValue();
                modelo.eliminar(c);
                break;
            }
            case "altaTiendaBtn": {
                Tienda t = new Tienda();
                t.setNombre(vista.txtNombreTienda.getText());
                t.setTelefono(vista.txtTlfTienda.getText());
                modelo.insertar(t);
                break;
            }
            case "modificarTiendaBtn": {
                Tienda t = (Tienda) vista.listTiendas.getSelectedValue();
                t.setNombre(vista.txtNombreTienda.getText());
                t.setTelefono(vista.txtTlfTienda.getText());
                modelo.modificar(t);
            }
            break;
            case "eliminarTiendaBtn": {
                Tienda t = (Tienda) vista.listTiendas.getSelectedValue();
                if (!comprobarTiendaVenta(t.getIdtienda())) {
                    JOptionPane.showMessageDialog(null, "Esta tienda está ligada a una venta, elimina primero la venta.",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                modelo.eliminar(t);
                break;
            }

            case "altaMarcaBtn": {
                Marca t = new Marca();
                t.setNombre(vista.txtNombreMarca.getText());
                t.setTelefono(vista.txtTelMarca.getText());
                modelo.insertar(t);
                break;
            }
            case "modificaMarcaBtn": {
                Marca t = (Marca) vista.listMarcas.getSelectedValue();
                t.setNombre(vista.txtNombreMarca.getText());
                t.setTelefono(vista.txtTelMarca.getText());
                modelo.modificar(t);
            }
            break;
            case "eliminarMarcaBtn": {
                Marca t = (Marca) vista.listMarcas.getSelectedValue();
                modelo.eliminar(t);
                break;
            }
            case "altaVentaBtn": {
                VentaProducto v = new VentaProducto();
                v.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                v.setProducto((Producto) vista.comboProductos.getSelectedItem());
                v.setTienda((Tienda) vista.comboTiendas.getSelectedItem());
                modelo.insertar(v);
                break;
            }
            case "modificarVentaBtn": {
                VentaProducto v = (VentaProducto) vista.listVentas.getSelectedValue();
                v.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                v.setProducto((Producto) vista.comboProductos.getSelectedItem());
                v.setTienda((Tienda) vista.comboTiendas.getSelectedItem());
                modelo.modificar(v);
                break;
            }
            case "eliminarVentaBtn": {
                VentaProducto v = (VentaProducto) vista.listVentas.getSelectedValue();
                modelo.eliminar(v);
                break;
            }
            case "listarMarcaProductosBtn": {
                Marca marca = (Marca) vista.listMarcas.getSelectedValue();
                listarMarcaProductos(modelo.getMarcaProductos(marca));
            }
        }
        limpiarCampos();
        actualizar();
    }

    /***
     * Comprobar si un producto está en el listado de ventas
     * @param id del producto
     * @return true si se puede borrar, false si no
     */
    private boolean comprobarProductoVenta(int id) {
        for (VentaProducto venta : modelo.getVentas()) {
            Producto p = venta.getProducto();
            if (p.getIdproducto() == id) {
                return false;
            }
        }
        return true;
    }

    /***
     * Comprobar si una tienda está en el listado de ventas
     * @param id de la tienda
     * @return true si se puede borrar, false si no
     */
    private boolean comprobarTiendaVenta(int id) {
        for (VentaProducto venta : modelo.getVentas()) {
            Tienda t = venta.getTienda();
            if (t.getIdtienda() == id) {
                return false;
            }
        }
        return true;
    }

    /***
     * Limpiar los campos
     */
    private void limpiarCampos() {
        limpiarCamposProductos();
        limpiarCamposClientes();
        limpiarCamposMarcas();
        limpiarCamposTiendas();
        limpiarCamposVentas();
    }

    /***
     * Limpiar los campos de las ventas
     */
    private void limpiarCamposVentas() {
        vista.comboProductos.setSelectedItem(-1);
        vista.comboTiendas.setSelectedItem(-1);
        vista.txtPrecio.setText("");
    }

    /***
     * Limpiar los campos de las tiendas
     */
    private void limpiarCamposTiendas() {
        vista.txtNombreTienda.setText("");
        vista.txtTlfTienda.setText("");
    }

    /***
     * Limpiar los campos de las marcas
     */
    private void limpiarCamposMarcas() {
        vista.txtNombreMarca.setText("");
        vista.txtTelMarca.setText("");
    }

    /***
     * Limpiar los campos de los clientes
     */
    private void limpiarCamposClientes() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidos.setText("");
        vista.txtDNI.setText("");
    }

    /***
     * Limpiar los campos de los productos
     */
    private void limpiarCamposProductos() {
        vista.txtNombre.setText("");
        vista.txtCodigo.setText("");
        vista.comboMarcas.setSelectedItem(-1);
    }

    /***
     * Actualizar los campos
     */
    private void actualizar() {
        listarProductos(modelo.getProductos());
        listarClientes(modelo.getClientes());
        listarTiendas(modelo.getTiendas());
        listarMarcas(modelo.getMarcas());
        listarVentas(modelo.getVentas());
    }

    /***
     * Listar los clientes
     * @param lista de clientes
     */
    public void listarClientes(ArrayList<Cliente> lista) {
        vista.dlmClientes.clear();
        for (Cliente c : lista) {
            vista.dlmClientes.addElement(c);
        }
    }

    /***
     * Listar las tiendas
     * @param lista de tiendas
     */
    private void listarTiendas(ArrayList<Tienda> lista) {
        vista.dlmTiendas.clear();
        for (Tienda t : lista) {
            vista.dlmTiendas.addElement(t);
        }
        vista.comboTiendas.removeAllItems();
        ArrayList<Tienda> tie = modelo.getTiendas();

        for (Tienda t : tie) {
            vista.comboTiendas.addItem(t);
        }
        vista.comboTiendas.setSelectedIndex(-1);
    }

    /***
     * Listar las marcas
     * @param lista de marcas
     */
    private void listarMarcas(ArrayList<Marca> lista) {
        vista.dlmMarcas.clear();
        for (Marca e : lista) {
            vista.dlmMarcas.addElement(e);
        }
        vista.comboMarcas.removeAllItems();
        ArrayList<Marca> ed = modelo.getMarcas();

        for (Marca e : ed) {
            vista.comboMarcas.addItem(e);
        }
        vista.comboMarcas.setSelectedIndex(-1);
    }

    /***
     * Listar los productos
     * @param lista de productos
     */
    public void listarProductos(ArrayList<Producto> lista) {
        vista.dlmProductos.clear();
        for (Producto unProducto : lista) {
            vista.dlmProductos.addElement(unProducto);
        }
        vista.comboProductos.removeAllItems();
        ArrayList<Producto> puz = modelo.getProductos();

        for (Producto p : puz) {
            vista.comboProductos.addItem(p);
        }
        vista.comboProductos.setSelectedIndex(-1);
    }

    /***
     * Listar las ventas
     * @param ventas lista de ventas
     */
    public void listarVentas(List<VentaProducto> ventas) {
        vista.dlmVentas.clear();
        for (VentaProducto venta : ventas) {
            vista.dlmVentas.addElement(venta);
        }

        vista.comboProductos.removeAllItems();
        ArrayList<Producto> p = modelo.getProductos();
        vista.comboTiendas.removeAllItems();
        ArrayList<Tienda> t = modelo.getTiendas();


        for (Producto producto : p) {
            vista.comboProductos.addItem(producto);
        }
        for (Tienda tienda : t) {
            vista.comboTiendas.addItem(tienda);
        }
        vista.comboProductos.setSelectedIndex(-1);
        vista.comboTiendas.setSelectedIndex(-1);
    }

    /***
     * Listar los productos que tiene una marca
     * @param lista de productos
     */
    public void listarMarcaProductos(List<Producto> lista) {
        vista.dlmTiendaProductos.clear();
        for (Producto unProducto : lista) {
            vista.dlmTiendaProductos.addElement(unProducto);
        }
    }

    /***
     * Añadir los listener
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.altaProductoBtn.addActionListener(listener);
        vista.eliminarProductoBtn.addActionListener(listener);
        vista.modificarProductoBtn.addActionListener(listener);
        vista.altaClienteBtn.addActionListener(listener);
        vista.modificarClienteBtn.addActionListener(listener);
        vista.eliminarClienteBtn.addActionListener(listener);
        vista.altaTiendaBtn.addActionListener(listener);
        vista.modificarTiendaBtn.addActionListener(listener);
        vista.eliminarTiendaBtn.addActionListener(listener);
        vista.altaMarcaBtn.addActionListener(listener);
        vista.modificarMarcaBtn.addActionListener(listener);
        vista.eliminarMarcaBtn.addActionListener(listener);
        vista.altaVentaBtn.addActionListener(listener);
        vista.modificarVentaBtn.addActionListener(listener);
        vista.eliminarVentaBtn.addActionListener(listener);
        vista.listarMarcaProductosBtn.addActionListener(listener);
    }

    /**
     * Añadir los listeners de las listas
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.listProductos.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listTiendas.addListSelectionListener(listener);
        vista.listMarcas.addListSelectionListener(listener);
        vista.listVentas.addListSelectionListener(listener);
        vista.listMarcaProductos.addListSelectionListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listProductos) {
                Producto productoSeleccion = (Producto) vista.listProductos.getSelectedValue();
                vista.txtNombre.setText(String.valueOf(productoSeleccion.getNombre()));
                vista.txtCodigo.setText(String.valueOf(productoSeleccion.getCodigo()));
                vista.comboMarcas.setSelectedItem(productoSeleccion.getMarca());
            }
            if (e.getSource() == vista.listClientes) {
                Cliente clienteSeleccionado = (Cliente) vista.listClientes.getSelectedValue();
                vista.txtNombreCliente.setText(String.valueOf(clienteSeleccionado.getNombre()));
                vista.txtApellidos.setText(String.valueOf(clienteSeleccionado.getApellidos()));
                vista.txtDNI.setText(String.valueOf(clienteSeleccionado.getDni()));
            }
            if (e.getSource() == vista.listTiendas) {
                Tienda t = (Tienda) vista.listTiendas.getSelectedValue();
                vista.txtNombreTienda.setText(String.valueOf(t.getNombre()));
                vista.txtTlfTienda.setText(String.valueOf(t.getTelefono()));
            }
            if (e.getSource() == vista.listMarcas) {
                Marca t = (Marca) vista.listMarcas.getSelectedValue();
                vista.txtNombreMarca.setText(String.valueOf(t.getNombre()));
                vista.txtTelMarca.setText(String.valueOf(t.getTelefono()));
            }
            if (e.getSource() == vista.listVentas) {
                VentaProducto vp = (VentaProducto) vista.listVentas.getSelectedValue();
                vista.comboProductos.setSelectedItem(vp.getProducto());
                vista.comboTiendas.setSelectedItem(vp.getTienda());
                vista.txtPrecio.setText(String.valueOf(vp.getPrecio()));
            }
        }

    }
}

