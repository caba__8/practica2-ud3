package com.alecaba.surf.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista que hereda de JFrame que permite la utilización de la interfaz
 */
public class Vista extends JFrame {
    private JPanel panel1;
    private JFrame frame;
    private JTabbedPane tabbedPane1;

    //Productos
    public JTextField txtNombre;
    public JTextField txtCodigo;
    public JComboBox comboMarcas;
    public JButton altaProductoBtn;
    public JButton modificarProductoBtn;
    public JButton eliminarProductoBtn;
    public JList listProductos;

    //Clientes
    public JTextField txtNombreCliente;
    public JTextField txtApellidos;
    public JTextField txtDNI;
    public JButton altaClienteBtn;
    public JButton modificarClienteBtn;
    public JButton eliminarClienteBtn;
    public JList listClientes;

    //Marcas
    public JTextField txtNombreMarca;
    public JTextField txtTelMarca;
    public JButton altaMarcaBtn;
    public JButton modificarMarcaBtn;
    public JButton eliminarMarcaBtn;
    public JList listMarcas;
    public JButton listarMarcaProductosBtn;
    public JList listMarcaProductos;

    //Tiendas
    public JTextField txtNombreTienda;
    public JTextField txtTlfTienda;
    public JButton altaTiendaBtn;
    public JButton modificarTiendaBtn;
    public JButton eliminarTiendaBtn;
    public JList listTiendas;

    //Venta
    public JComboBox comboProductos;
    public JComboBox comboTiendas;
    public JTextField txtPrecio;
    public JButton altaVentaBtn;
    public JButton modificarVentaBtn;
    public JButton eliminarVentaBtn;
    public JList listVentas;

    DefaultListModel dlmProductos;
    DefaultListModel dlmClientes;
    DefaultListModel dlmTiendas;
    DefaultListModel dlmMarcas;

    //Filtros
    DefaultListModel dlmTiendaProductos;
    DefaultListModel dlmVentas;

    JMenuItem conexionItem;
    JMenuItem salirItem;


    /**
     * Constructor de clase vacío que inizializa lo necesario
     */
    public Vista(){
        frame = new JFrame("Tienda Surf");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);
        frame.setSize(new Dimension(800,430));
        frame.setLocationRelativeTo(null);

        crearMenu();
        crearModelos();

    }

    /**
     * Método que permite crear los modelos, es decir añadir DLM a las listas
     */
    private void crearModelos() {
        dlmProductos = new DefaultListModel();
        listProductos.setModel(dlmProductos);

        dlmClientes = new DefaultListModel();
        listClientes.setModel(dlmClientes);

        dlmTiendas = new DefaultListModel();
        listTiendas.setModel(dlmTiendas);

        dlmMarcas = new DefaultListModel();
        listMarcas.setModel(dlmMarcas);

        dlmTiendaProductos = new DefaultListModel();
        listMarcaProductos.setModel(dlmTiendaProductos);

        dlmVentas = new DefaultListModel();
        listVentas.setModel(dlmVentas);

    }

    /**
     * Método que permite crear un menú y añadir actionCommands
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
