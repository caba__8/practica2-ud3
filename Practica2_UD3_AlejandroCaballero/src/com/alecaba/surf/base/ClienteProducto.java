package com.alecaba.surf.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "cliente_producto", schema = "tiendasurf", catalog = "")
public class ClienteProducto {
    private int idclienteproducto;

    @Id
    @Column(name = "idclienteproducto")
    public int getIdclienteproducto() {
        return idclienteproducto;
    }

    public void setIdclienteproducto(int idclienteproducto) {
        this.idclienteproducto = idclienteproducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteProducto that = (ClienteProducto) o;
        return idclienteproducto == that.idclienteproducto;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idclienteproducto);
    }
}
