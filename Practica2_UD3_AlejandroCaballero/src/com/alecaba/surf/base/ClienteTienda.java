package com.alecaba.surf.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "cliente_tienda", schema = "tiendasurf", catalog = "")
public class ClienteTienda {
    private int idclientetienda;

    @Id
    @Column(name = "idclientetienda")
    public int getIdclientetienda() {
        return idclientetienda;
    }

    public void setIdclientetienda(int idclientetienda) {
        this.idclientetienda = idclientetienda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteTienda that = (ClienteTienda) o;
        return idclientetienda == that.idclientetienda;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idclientetienda);
    }
}
