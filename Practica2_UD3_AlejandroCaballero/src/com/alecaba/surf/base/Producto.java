package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Producto {
    private int idproducto;
    private String nombre;
    private String codigo;
    private List<Cliente> clientes;
    private List<VentaProducto> ventas;
    private Marca marca;

    @Id
    @Column(name = "idproducto")
    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return idproducto == producto.idproducto &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(codigo, producto.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idproducto, nombre, codigo);
    }

    @ManyToMany(mappedBy = "productos")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @OneToMany(mappedBy = "producto")
    public List<VentaProducto> getVentas() {
        return ventas;
    }

    public void setVentas(List<VentaProducto> ventas) {
        this.ventas = ventas;
    }

    @ManyToOne
    @JoinColumn(name = "idmarca", referencedColumnName = "idmarca")
    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return nombre + " - " + codigo + " - " + marca;
    }
}
