package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int idcliente;
    private String nombre;
    private String apellidos;
    private String dni;
    private List<Tienda> tiendas;
    private List<Producto> productos;

    @Override
    public String toString() {
        return nombre + " - " + apellidos + " - " + dni;
    }

    @Id
    @Column(name = "idcliente")
    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return idcliente == cliente.idcliente &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(dni, cliente.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcliente, nombre, apellidos, dni);
    }

    @ManyToMany
    @JoinTable(name = "cliente_tienda", catalog = "", schema = "tiendaproductos", joinColumns = @JoinColumn(name = "idcliente", referencedColumnName = "idcliente"), inverseJoinColumns = @JoinColumn(name = "idtienda", referencedColumnName = "idtienda"))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    @ManyToMany
    @JoinTable(name = "cliente_producto", catalog = "", schema = "tiendaproductos", joinColumns = @JoinColumn(name = "idcliente", referencedColumnName = "idcliente"), inverseJoinColumns = @JoinColumn(name = "idproducto", referencedColumnName = "idproducto"))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
