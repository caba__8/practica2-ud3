package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "producto_tienda", schema = "tiendasurf", catalog = "")
public class VentaProducto {
    private int idproductotienda;
    private Float precio;
    private Producto producto;
    private Tienda tienda;

    @Override
    public String toString() {
        return producto.getNombre() + " - " + tienda.getNombre() + " - " + precio;
    }

    @Id
    @Column(name = "idproductotienda")
    public int getIdproductotienda() {
        return idproductotienda;
    }

    public void setIdproductotienda(int idproductotienda) {
        this.idproductotienda = idproductotienda;
    }

    @Basic
    @Column(name = "precio")
    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VentaProducto that = (VentaProducto) o;
        return idproductotienda == that.idproductotienda &&
                Objects.equals(precio, that.precio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idproductotienda, precio);
    }

    @ManyToOne
    @JoinColumn(name = "idproducto", referencedColumnName = "idproducto")
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @ManyToOne
    @JoinColumn(name = "idtienda", referencedColumnName = "idtienda")
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
}
