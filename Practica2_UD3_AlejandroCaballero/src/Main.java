import com.alecaba.surf.gui.Controlador;
import com.alecaba.surf.gui.Modelo;
import com.alecaba.surf.gui.Vista;

/***
 * Clase Principal
 */
public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, vista);
    }
}